# SpotWelder 1-2-3

## Arduino Spot Welder Control Firmware
Control firmware for the SpotWelder 1-2-3 resistance spot welder. Implements accurate double pulse welding control on an Arduino Pro Mini, with a simple UI based on an I2C OLED display, a rotary encoder, a push button, and a led.

A welding cycle is composed of a warm-up step, a press step, and a welding step. A set of duration values for the three steps is a welding profile. Materials of different thickness require different welding profiles, and it is very handy to be able to store and recall profiles for repeated use.
The firmware supports permanent storage of several welding profiles in flash memory.

The firmware also supports standby mode for the welder. Standby mode is automatically entered after a pre-configured period of user inactivity. In standby mode:
- The OLED display is switched off to avoid burn in and maximize display life. This is the main purpose of the standby mode.
- The welding led fades in and out to signal standby mode.
- The welding button and the encoder button are disabled.
- The encoder dial is enabled and can be rotated left or right to exit standby and reactivate the display.

The encoder is used to navigate the menu structure (rotate left/right), select a menu item or confirm a value for storage (press), move up one level (press and held), view the firmware version (double click). 

Pressing the welding button starts a welding cycle. A digital output pin is used to provide a signal to drive the power circuit (often an optocoupler combined with a power solid state device). The power circuit switches the welding transformer on and off.

## Inputs
- Pins A0 A1 A2 to a rotary encoder with push button used to select/change profiles and parameters.
- Pin 2 connected to a temporary push button that is pressed to start welding. The push button is normally mounted to a panel next to the encoder, and can be connected in parallel to a pedal switch.

## Outputs
- Pins A4/A5 for I2C controlling the display.
- Pin 11 for digital output to a LED, which is switched on during a welding cycle, and faded in and out during standby.
- Pin 12 for digital output to the mains power circuit (input to power circuit). This pin is normally LOW and will go HIGH-LOW-HIGH during a welding cycle. 

## Author
mecanicafina, mecanicafinam@gmail.com

## License

The control firmware for the SpotWelder 1-2-3 is available under the MIT License. See the LICENSE file for more info.

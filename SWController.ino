#include <RBD_Timer.h>
#include <ClickEncoder.h>
#include <TimerOne.h>
#include <MenuSystem.h>
#include <EEPROMex.h>


#define DEBUG
#define SWVERSION "1.1"
/*******************************
 * Defaults *
 *******************************/
// min-max pulse durations in seconds
# define WARMUP_MIN   0.20
# define WARMUP_MAX   5.00
# define PRESS_MIN    0.20
# define PRESS_MAX    2.00
# define WELD_MIN     0.20
# define WELD_MAX     5.00

# define STANDBYDELAY 120000     // standby, default inactivity period before entering standby, msec

// less likely one may want to change these, but here they are 
// pin assignments
# define WELDBUTTON   2
# define WELDLED      11
# define OPTO         12

# define ENCBUTTON    A2
# define ENCLEFT      A0
# define ENCRIGHT     A1

# define PULSE_INC   0.2        // pulse duration increment on UI, seconds

# define EEPROM_BASE 63         // base address for parameter defaults storage
# define DEFAULT_DURATION 1.0   // default value for pulse durations, actually used only when the EEPROM does not contain defaults (on first use)

# define FADINGSTART 0          // standby, starting brightness for led fading
# define FADINGDELTA 5          // standby, brightness increment for led fading

/*******************************
 * Utility macros *
 *******************************/
#ifdef DEBUG
// serial for debugging
HardwareSerial &dbgPort = Serial;
#endif

// only if debugging - prints on the debug port
#ifdef DEBUG
# define DEBUG_PRINT(x) dbgPort.print(x)
# define DEBUG_PRINTLN(x) dbgPort.print(x); dbgPort.print('\n');
#else
# define DEBUG_PRINT(x) do {} while (0)
# define DEBUG_PRINTLN(x) do {} while (0)
#endif

/*******************************
 * display & UI *
 *******************************/
#define I2C_ADDRESS 0x3C
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

SSD1306AsciiAvrI2c oled;

void clearLine(uint8_t line) {
  oled.clear(0,oled.displayWidth(),line,line);
}

// displays status messages left portion of row 4
void displayMsg (String msg) {
  // clear only msg area
  oled.clear(0,101,3,3);
  oled.setCursor(0,3);
  oled.print(msg);
}

// forward declare the menu system variable, we need to reference here
extern MenuSystem ms;
// flags the current pulse duration during welding, optionally removing the flag on the previous duration (pulses 2-3 only)
// we do this only if displaying a top level menu
void flagDuration (uint8_t line, bool removePrevious=false) {
  // return if we are not in one of the top level menus named "0", "1",...
  // just check that the length of the menu name is 1
  if ( strlen(ms.get_current_menu()->get_current_component()->get_name()) > 1 ) 
    return;
  // clear only msg area
  if ( removePrevious) oled.clear(96,101,line-1,line-1);
  oled.setCursor(96,line);
  // print a -> character
  oled.println(char(126));
}

// forward declare as we need to reference it in the next function
extern uint8_t activeProfileNumber;

// durations are always fetched from stored defaults as the used may alter but not save a parameter
// in this case the correct behavior is to show the stored parameter at top level
inline void displayDurationsForActiveProfile () {
   // use rows 1-2-3 from column 102
   for (uint8_t i=1; i<4; i++) {
    oled.setCursor(102,i);
    oled.print(readDefault(activeProfileNumber, i-1));
  }
}

/// clear the durations display area
inline void clearDurations () {
  oled.clear(102,oled.displayWidth(),1,3);
}

// profile menus are named "0", "1",... (rendered as "Profile 1", "Profile 2"... in display)
// maps "0" to "Profile 0", etc. for display
inline String displayMenuName (const char* menuName) {
  return ("Profile " + String(atoi(menuName) +1));
}

// menu system
class MyRenderer : public MenuComponentRenderer {
public:
// call sequence is always render, then either render_menu (shows "Profile n") or render_numeric_menu_item (shows pulse menu item)
    void render(Menu const& menu) const {
        DEBUG_PRINTLN("[rendering, menu name=<" + String(menu.get_name()) +">]");
        // clear line 1 here and print menu name
        // menu.get_name() returns null string when current_component is render_menu, "n" when current_component is render_numeric_menu_item
        clearLine(1);
        if (menu.get_name() != "") {
          oled.setCursor(0,1);
          oled.print(displayMenuName(menu.get_name()));
        }
        oled.setCursor(0,2);
        menu.get_current_component()->render(*this);
    }

    // shows profile menu and current durations, sets active profile
    void render_menu(Menu const& menu) const {
      DEBUG_PRINTLN("[render_menu]");
      clearLine(2);
      activeProfileNumber = (uint8_t) atoi(menu.get_name());
      oled.print(displayMenuName(menu.get_name()));
      //displayDurations (menu);
      displayDurationsForActiveProfile();
    } 

    void render_numeric_menu_item(NumericMenuItem const& menu_item) const {
      DEBUG_PRINTLN("[render_numeric_menu_item]");
      clearDurations();
      clearLine(2);
      String buffer;
      buffer = menu_item.get_name();
      buffer += menu_item.has_focus() ? '<' : '=';
      buffer += menu_item.get_formatted_value();
      if (menu_item.has_focus())
          buffer += '>';
      oled.print(buffer);
    }

    void render_menu_item(MenuItem const& menu_item) const {};
    void render_back_menu_item(BackMenuItem const& menu_item) const {};
    
//    // unused in this version - 
//    void render_menu_item(MenuItem const& menu_item) const {
//      Serial.println("render_menu_item");
//      clearDurations();
//      clearLine(2);
//      oled.print(menu_item.get_name());
//    }
    
//    // unused in this version - there is no BackMenuItem, backing is by encoder long press
//    void render_back_menu_item(BackMenuItem const& menu_item) const {
//      Serial.println("render_back_menu_item");
//      clearLine(2);
//      oled.print(menu_item.get_name());
//    }

};


// menu renderer global var
MyRenderer my_renderer;
// Forward declaration
void on_selected(MenuComponent* p_menu_component);
// Menu variables
MenuSystem ms(my_renderer);
Menu P1("0"); // menu
NumericMenuItem P1_wu("Warm-up pulse", on_selected, DEFAULT_DURATION, WARMUP_MIN, WARMUP_MAX, PULSE_INC, nullptr);
NumericMenuItem P1_pr("Press pulse", on_selected, DEFAULT_DURATION, PRESS_MIN, PRESS_MAX, PULSE_INC, nullptr);
NumericMenuItem P1_we("Weld pulse", on_selected, DEFAULT_DURATION, WELD_MIN, WELD_MAX, PULSE_INC, nullptr);
Menu P2("1");
NumericMenuItem P2_wu("Warm-up pulse", on_selected, DEFAULT_DURATION, WARMUP_MIN, WARMUP_MAX, PULSE_INC, nullptr);
NumericMenuItem P2_pr("Press pulse", on_selected, DEFAULT_DURATION, PRESS_MIN, PRESS_MAX, PULSE_INC, nullptr);
NumericMenuItem P2_we("Weld pulse", on_selected, DEFAULT_DURATION, WELD_MIN, WELD_MAX, PULSE_INC, nullptr);
Menu P3("2");
NumericMenuItem P3_wu("Warm-up pulse", on_selected, DEFAULT_DURATION, WARMUP_MIN, WARMUP_MAX, PULSE_INC, nullptr);
NumericMenuItem P3_pr("Press pulse", on_selected, DEFAULT_DURATION, PRESS_MIN, PRESS_MAX, PULSE_INC, nullptr);
NumericMenuItem P3_we("Weld pulse", on_selected, DEFAULT_DURATION, WELD_MIN, WELD_MAX, PULSE_INC, nullptr);


// Menu callback function
void on_selected(MenuComponent* p_menu_component) {
  DEBUG_PRINT(("[Profile] "));DEBUG_PRINT((ms.get_current_menu()->get_name()));
  DEBUG_PRINT((" [Pulse] "));DEBUG_PRINT((ms.get_current_menu()->get_current_component_num()));
  NumericMenuItem* nmi = (NumericMenuItem*) p_menu_component;
  DEBUG_PRINT((" [Saved value ] "));DEBUG_PRINTLN((nmi->get_value()));
  // save value to EEPROM
  //saveDefault(atoi(ms.get_current_menu()->get_name()), ms.get_current_menu()->get_current_component_num(), ((NumericMenuItem*) p_menu_component)->get_value());
  saveDefault(atoi(ms.get_current_menu()->get_name()), ms.get_current_menu()->get_current_component_num(), nmi->get_value());
}


/*******************************
 * Pin assignement, global state, welding & defaults storage*
 *******************************/
// welding button
const int weldButtonPin = WELDBUTTON;
const int weldLedPin = WELDLED;              // digital pin output for welding led

const int optoPin = OPTO;     // digital pin output for optocoupler

// Timer is used to time the welding pulse sequence H-L-H
volatile RBD::Timer wTimer;

volatile unsigned int state = 0;  // current welder state and pulse number

uint8_t activeProfileNumber = 0;  // current profile number

// array holding the durations for the welding process, loaded from active profile
// duration[i] = duration for state i (index 0 unused) 
// in msec
volatile unsigned long durations[] = {0, 1000, 2000, 3000};

// argument ranges: profile 0-1-2, pulse 0-1-2
void saveDefault(uint8_t profile, uint8_t pulse, float value) {
  int address = EEPROM_BASE + (profile * 3 + pulse) * sizeof(float);
  EEPROM.updateFloat(address, value);
}

float readDefault(uint8_t profile, uint8_t pulse) {
  int address = EEPROM_BASE + (profile * 3 + pulse) * sizeof(float);
  float value = EEPROM.readFloat(address);
  //DEBUG_PRINT(("[Default read ")); DEBUG_PRINT(((profile) + pulse)); DEBUG_PRINT(("] = ")); DEBUG_PRINT((value));
  // this to avoid returning garbage on first use 
  if ( isnan( value ) ) 
    return DEFAULT_DURATION;
  else 
    return (value);
}


/*******************************
 * Standy *
 *******************************/
// Timer used to enter standby
RBD::Timer UITimer;
// State variable for UI state
bool UIActive = true;
// welding led fading during standby
int weldLedBrightness = FADINGSTART;    // brightness of welding led for standby fading
int brightnessDelta = FADINGDELTA;      // brightness increment for standby fading
const int fadingDelay = 10;             // standby fading delay, msec


/*******************************
 * Encoder *
 *******************************/
// encoder
ClickEncoder *encoder;
int16_t last, value;

void timerIsr() {
  encoder->service();
}


/*******************************
 * Setup *
 *******************************/
void setup(void)
{
  //display
  oled.begin(&Adafruit128x32, I2C_ADDRESS);
  oled.setScroll(false);
  oled.setFont(System5x7);
  oled.clear();
  oled.println("Spot Welder 1-2-3");
  
  // menus
  ms.get_root_menu().add_menu(&P1);
  P1.add_item(&P1_wu);
  P1_wu.set_value(readDefault(0, 0));
  P1.add_item(&P1_pr);
  P1_pr.set_value(readDefault(0, 1));
  P1.add_item(&P1_we);
  P1_we.set_value(readDefault(0, 2));
  
  ms.get_root_menu().add_menu(&P2);
  P2.add_item(&P2_wu);
  P2_wu.set_value(readDefault(1, 0));
  P2.add_item(&P2_pr);
  P2_pr.set_value(readDefault(1, 1));
  P2.add_item(&P2_we);
  P2_we.set_value(readDefault(1, 2));
  
  ms.get_root_menu().add_menu(&P3);
  P3.add_item(&P3_wu);
  P3_wu.set_value(readDefault(2, 0));
  P3.add_item(&P3_pr);
  P3_pr.set_value(readDefault(2, 1));
  P3.add_item(&P3_we);
  P3_we.set_value(readDefault(2, 2));
  //ms.prev();
  ms.display();
  //encoder
  encoder = new ClickEncoder(ENCLEFT, ENCRIGHT, ENCBUTTON,4);
  encoder->setAccelerationEnabled(true);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr); 
  last = -1;
  // optocoupler
  pinMode(optoPin, OUTPUT);
  digitalWrite(optoPin, LOW);
  state = 0;
  // weld button and led
  pinMode(weldButtonPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(weldButtonPin), startWelding, RISING);
  pinMode(weldLedPin, OUTPUT);
  digitalWrite(weldLedPin, LOW);

  // set UI standby timeout value
  UITimer.setTimeout(STANDBYDELAY);
  UITimer.restart();
  
  #ifdef DEBUG
  dbgPort.begin(9600);
  #endif
  
}


/*******************************
 * Core functions *
 *******************************/

void activateUI() {
  oled.ssd1306WriteCmd(SSD1306_DISPLAYON);
  digitalWrite(weldLedPin, LOW);
  UIActive = true;
  DEBUG_PRINTLN(("[-------------------UI on] "));
}

void deActivateUI() {
  oled.ssd1306WriteCmd(SSD1306_DISPLAYOFF);
  // reset led fading parameters to defaults going into standby
  weldLedBrightness = FADINGSTART;
  brightnessDelta = FADINGDELTA;
  UIActive = false;
  DEBUG_PRINTLN(("[-------------------UI off] "));
}

void readEncoder() {
  // position reading
  value += encoder->getValue();
  bool didResume = false;
  if (value != last) {
    //DEBUG_PRINT(("[-------------------timer] "));DEBUG_PRINTLN((UITimer.getValue()));
    if (UITimer.isExpired()) {
        // user is active and display is off, activate UI
        activateUI();
        // do not use the current encoder reading, so rotating the encoder to exit standy will not change the UI state
        value = last;
        didResume = true;
    }
    // user is active, restart display timeout counter
    UITimer.restart();
    //DEBUG_PRINTLN(("[-------------------Standby timer restarted] "));
    if ( !didResume ) {
      // ToDo - activate buzzer
      //tone(9,1000,3);
      if ( value > last ) {
        ms.next(true);      
        }
      else {
        ms.prev(true);   
        }
      ms.display(); 
      last = value;
    }
    DEBUG_PRINT(("[Encoder value] ")); DEBUG_PRINTLN((value));
  }
  else {
    if ( UITimer.onExpired() )
      // user is inactive and timeout has expired, deactivate UI
      deActivateUI();
  }
  // button reading
  ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open && UIActive) {
    UITimer.restart();
    //DEBUG_PRINTLN(("[-------------------Standby timer restarted] "));
    DEBUG_PRINT(("[EncoderButton] "));
    #define VERBOSECASE(label) case label: DEBUG_PRINTLN((#label)); break;
    switch (b) {
      case ClickEncoder::Clicked:
        DEBUG_PRINTLN(("Clicked"));
        ms.select();
        ms.display();
        break;
      case ClickEncoder::Released:
        DEBUG_PRINTLN(("Released"));
        ms.back();
        ms.display();
        break;
      case ClickEncoder::Held:
        DEBUG_PRINTLN(("Held"));
        break;
      case ClickEncoder::DoubleClicked:
        // SWVERSION
        DEBUG_PRINTLN(("DoubleClicked"));
        oled.setFont(Wendy3x5);
        displayMsg("v" + String(SWVERSION) + " Mecanicafina 2017");
        oled.setFont(System5x7);
        break;
    }
  }
}

// Welding is a 3 steps process (warm up - press - weld), optocoupler output (optoPin) goes HIGH-LOW-HIGH 
// The states are coded as 0-1-2-3 are and cycled as follows:
// 0: not welding, optoPin LOW
// 1: warm-up pulse, optoPin HIGH, first warm-up welding pulse, milliseconds
// 2: press pulse, optoPin LOW, welding pause, milliseconds
// 3: weld pulse, optoPin HIGH, second final welding pulse, milliseconds
// 0: back to not welding

void startWelding(void)
{ 
  if ( (state == 0) && UIActive ) {
    DEBUG_PRINT(("Welding\n"));
    state = 1;
    // stop the standby timer while welding
    UITimer.stop();
    displayMsg("Welding..." + String(state));
    // collect current profile durations and load array in msec
    
    DEBUG_PRINT(("Profile "));DEBUG_PRINTLN((activeProfileNumber));
    for (uint8_t i=0; i<3; i++) {
       durations[i+1] = (unsigned long) (1000 * readDefault(activeProfileNumber, i));
       DEBUG_PRINT(("[durations] ")); DEBUG_PRINTLN((durations[i+1]));
    }
    wTimer.setTimeout(durations[state]);
    wTimer.restart();
    digitalWrite(optoPin, HIGH);
    digitalWrite(weldLedPin, HIGH);
    flagDuration(state);
    
    DEBUG_PRINT(("[state] ")); DEBUG_PRINT((state)); DEBUG_PRINT((" [pulse] ")); DEBUG_PRINT((digitalRead(optoPin)));
    DEBUG_PRINT((" [duration] ")); DEBUG_PRINTLN((wTimer.getTimeout()));
  }
}


void loop(void)
{   
  switch ( state ) {
    // not welding
    case 0:
      readEncoder();
      if ( !UIActive ) {
        analogWrite(weldLedPin, weldLedBrightness);
        weldLedBrightness += brightnessDelta;
        // effect is nicer if we don't go to full brightness
        if (weldLedBrightness <= 0 || weldLedBrightness >= 169) {
          brightnessDelta = -brightnessDelta;
        }
        // insert a delay to make fading effect visible to user
        delay(40);
      }
      break;

    // welding
    case 1:
    case 2:
    case 3:
      if (wTimer.onExpired()) {
        // timer just expired, go for new state in the sequence 1-2-3-0
        state = (state + 1) % 4;
        digitalWrite(optoPin, !digitalRead(optoPin));
        // if we are in state > 0 start the next pulse else just cancel pulse info
        if ( state > 0 ) {
          wTimer.setTimeout(durations[state]);
          wTimer.restart();
          displayMsg("Welding..." + String(state));
          flagDuration(state, true);
          
          DEBUG_PRINT(("[state] ")); DEBUG_PRINT((state)); DEBUG_PRINT((" [pulse] ")); DEBUG_PRINT((digitalRead(optoPin)));
          DEBUG_PRINT((" [duration] ")); DEBUG_PRINTLN((wTimer.getTimeout()));
        }
        else {
          digitalWrite(weldLedPin, LOW);
          displayMsg("");
          // finished welding, restart the standby timer
          UITimer.restart();
          DEBUG_PRINTLN(("Done."));
        }   
      }
      break;
         
    default:
      break;
    
  }
}
